import { createRouter, createWebHistory } from 'vue-router'
import RegisterStep1View from '../views/RegisterStep1View.vue'
import RegisterStep2View from '../views/RegisterStep2View.vue'
import RegisterStep31View from '../views/RegisterStep31View.vue'
import RegisterStep32View from '../views/RegisterStep32View.vue'
import RegisterStep33View from '../views/RegisterStep33View.vue'
import RegisterStep4View from '../views/RegisterStep4View.vue'
import ParticipantView from '../views/Admin/ParticipantView.vue'
import ParticipantDetailView from '../views/Admin/ParticipantDetailView.vue'

const routes = [
  {
    path: '/register/step-1',
    name: 'register-step-1',
    component: RegisterStep1View
  },
  {
    path: '/register/step-2',
    name: 'register-step-2',
    component: RegisterStep2View
  },
  {
    path: '/register/step-3-1',
    name: 'register-step-3-1',
    component: RegisterStep31View
  },
  {
    path: '/register/step-3-2',
    name: 'register-step-3-2',
    component: RegisterStep32View
  },
  {
    path: '/register/step-3-3',
    name: 'register-step-3-3',
    component: RegisterStep33View
  },
  {
    path: '/register/step-4',
    name: 'register-step-4',
    component: RegisterStep4View
  },
  {
    path: '/admin/participant',
    name: 'participant',
    component: ParticipantView
  },
  {
    path: '/admin/participant/:id/detail',
    name: 'participant-detail',
    component: ParticipantDetailView
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
